import { Component, OnInit } from '@angular/core';
import { PopularEventsService } from './popular-events.service';
import { Observable } from 'rxjs';
import { Event } from '../shared/event.model';
import { tap } from 'rxjs/operators';
import { LoadingInticatorService } from '../shared/loading-inticator/loading-inticator.service';

@Component({
  selector: 'app-popular-events',
  templateUrl: './popular-events.component.html',
  styleUrls: ['./popular-events.component.scss']
})
export class PopularEventsComponent implements OnInit {
  popularEvents: Observable<Event[]>;

  constructor(
    private popularEventsService: PopularEventsService,
    private loadingInticatorService: LoadingInticatorService
  ) { }

  ngOnInit() {
    this.loadingInticatorService.setLoading(true);
    this.popularEvents = this.popularEventsService.getPopularEvents()
      .pipe(tap(() => this.loadingInticatorService.setLoading(false)));
  }

  trackEvent(index: number, event: Event) {
    return event.id;
  }
}
