import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopularEventsComponent } from './popular-events.component';
import { PopularEventsService } from './popular-events.service';
import { Component, Input } from '@angular/core';
import { of } from 'rxjs';

describe('PopularEventsComponent', () => {
  let component: PopularEventsComponent;
  let fixture: ComponentFixture<PopularEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PopularEventsComponent,
        MockPopularEventComponent
      ],
      providers: [
        { provide: PopularEventsService, useValue: { getPopularEvents: () => of([]) } }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopularEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector: 'app-popular-event',
  template: `<div>app-popular-event</div>`
})
class MockPopularEventComponent {
  @Input() event;
}