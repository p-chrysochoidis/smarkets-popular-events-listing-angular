import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Event } from 'src/app/shared/event.model';
import { EventDetails } from '../../shared/event-details.model';

@Component({
  selector: 'app-popular-event',
  templateUrl: './popular-event.component.html',
  styleUrls: ['./popular-event.component.scss']
})
export class PopularEventComponent implements OnChanges {
  @Input() event: Event;
  eventDetails: EventDetails;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['event']) {
      this.eventDetails = new EventDetails(this.event);
    }
  }
}
