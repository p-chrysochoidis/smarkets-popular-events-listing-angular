import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopularEventComponent } from './popular-event.component';
import { SimpleChange } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('PopularEventComponent', () => {
  let component: PopularEventComponent;
  let fixture: ComponentFixture<PopularEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopularEventComponent ],
      imports: [
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopularEventComponent);
    component = fixture.componentInstance;
    component.event = <any>{
      id: 'e1',
      name: 'name',
      short_name: 'sname',
      state: 'state',
      type: 'type'
    };
    component.ngOnChanges({event: new SimpleChange(null, component.event, true)})
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
