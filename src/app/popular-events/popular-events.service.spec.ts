import { TestBed } from '@angular/core/testing';

import { PopularEventsService } from './popular-events.service';
import { ApiService } from '../shared/api.service';
import { ApiServiceMock } from 'src/mocks/api-service.mock';

describe('PopularEventsService', () => {
  let service: PopularEventsService;
  let apiMock: ApiServiceMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: ApiService, useClass: ApiServiceMock }
      ]
    });
    service = TestBed.get(PopularEventsService);
    apiMock = TestBed.get(ApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getPopularEvents', () => {
    it('should return an empty array when eventids is empty', () => {
      let theEvents = null;
      service.getPopularEvents().subscribe((events) => theEvents = events);
      apiMock.emmitPopularEventIds([]);
      expect(theEvents).toEqual([]);
    });

    it('should emmit an empty array for events that does not exist', () => {
      let theEvents = null;
      service.getPopularEvents().subscribe((events) => theEvents = events);
      apiMock.emmitPopularEventIds(['e1', 'e2', 'e3']);
      apiMock.emmitEventsByIds([]);
      expect(theEvents).toEqual([]);
    });

    it('should emmit the events', () => {
      let theEvents = null;
      service.getPopularEvents().subscribe((events) => theEvents = events);
      apiMock.emmitPopularEventIds(['e1', 'e2']);
      apiMock.emmitEventsByIds(<any>[{id: 'e1'}, {id: 'e2'}]);
      expect(theEvents).toEqual([{id: 'e1'}, {id: 'e2'}]);
    });
  });
});
