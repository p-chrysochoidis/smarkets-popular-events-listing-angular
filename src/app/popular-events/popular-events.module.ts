import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PopularEventsRoutingModule } from './popular-events-routing.module';
import { PopularEventsComponent } from './popular-events.component';
import { PopularEventComponent } from './popular-event/popular-event.component';

@NgModule({
  declarations: [PopularEventsComponent, PopularEventComponent],
  imports: [
    CommonModule,
    PopularEventsRoutingModule
  ]
})
export class PopularEventsModule { }
