import { Injectable } from '@angular/core';
import { ApiService } from '../shared/api.service';
import { Observable, of } from 'rxjs';
import { Event } from '../shared/event.model';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PopularEventsService {
  constructor(private apiService: ApiService) { }

  public getPopularEvents(): Observable<Event[]> {
    return this.apiService.getPopularEventIds()
      .pipe(mergeMap((eventIds) => {
        if(eventIds.length) {
          return this.apiService.getEventsById(eventIds);
        }
        return of([]);
      }));
  }
}
