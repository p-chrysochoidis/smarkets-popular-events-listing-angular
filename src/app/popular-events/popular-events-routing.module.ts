import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PopularEventsComponent } from './popular-events.component';

const routes: Routes = [
  { path: 'popular-events', component: PopularEventsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PopularEventsRoutingModule { }
