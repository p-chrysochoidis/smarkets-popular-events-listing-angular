import { Event } from "../shared/event.model";
import { Market } from "../shared/market.model";

export interface EventWithMarkets extends Event {
    markets: Market[];
}