import { Injectable } from '@angular/core';
import { ApiService } from '../shared/api.service';
import { combineLatest, map } from 'rxjs/operators';
import { EventWithMarkets } from './event-with-markets.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventInfoService {

  constructor(private apiService: ApiService) { }

  public getEventInfo(eventId: string): Observable<EventWithMarkets> {
    return this.apiService.getEventsById([eventId])
      .pipe(
        combineLatest(this.apiService.getEventsMarkets([eventId])),
        map(([events, markets]) => {
          if (events.length) {
            return {...events[0], markets};
          }
          return null;
        })
      );
  }
}
