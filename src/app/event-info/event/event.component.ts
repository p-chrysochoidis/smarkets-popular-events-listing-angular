import { Component, OnInit } from '@angular/core';
import { EventInfoService } from '../event-info.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { EventWithMarkets } from '../event-with-markets.model';
import { switchMap, tap, map } from 'rxjs/operators';
import { LoadingInticatorService } from 'src/app/shared/loading-inticator/loading-inticator.service';
import { EventDetails } from 'src/app/shared/event-details.model';
import { Market } from 'src/app/shared/market.model';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {
  eventWithMarkets: Observable<{data: EventWithMarkets, details: EventDetails}>;

  constructor(
    private eventInfoService: EventInfoService,
    private route: ActivatedRoute,
    private loadingInticatorService: LoadingInticatorService
  ) { }

  ngOnInit() {
    this.loadingInticatorService.setLoading(true);
    this.eventWithMarkets = this.route.paramMap.pipe(
      switchMap((params) => this.eventInfoService.getEventInfo(params.get('id'))),
      map((event) => { return {data: event, details: new EventDetails(event)}; }),
      tap(() => this.loadingInticatorService.setLoading(false))
    )
  }

  public toUserDate(date: string): string {
    return new Date(date).toLocaleString();
  }

  public trackByMarketId(index: number, market: Market): string {
    return market.id;
  }
}
