import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventInfoRoutingModule } from './event-info-routing.module';
import { EventComponent } from './event/event.component';

@NgModule({
  declarations: [EventComponent],
  imports: [
    CommonModule,
    EventInfoRoutingModule
  ]
})
export class EventInfoModule { }
