import { TestBed } from '@angular/core/testing';

import { EventInfoService } from './event-info.service';
import { ApiService } from '../shared/api.service';
import { ApiServiceMock } from 'src/mocks/api-service.mock';

describe('EventInfoService', () => {
  let service: EventInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: ApiService, useClass: ApiServiceMock }
      ]
    });
    service = TestBed.get(EventInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
