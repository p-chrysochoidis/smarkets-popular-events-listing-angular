export interface Event {
    bettable: boolean;
    created: string;
    description: string;
    display_order: 0;
    end_date: string;
    full_slug: string;
    id: string;
    inplay_enabled: boolean;
    modified: string;
    name: string;
    parent_id: string;
    short_name: string;
    slug: string;
    special_rules: string;
    start_date: string;
    start_datetime: string;
    state: string;
    type: string;
  }