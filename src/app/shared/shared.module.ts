import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingInticatorComponent } from './loading-inticator/loading-inticator.component';

@NgModule({
  declarations: [
    LoadingInticatorComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LoadingInticatorComponent
  ]
})
export class SharedModule { }
