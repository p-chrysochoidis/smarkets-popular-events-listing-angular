import { Event } from './event.model';

export interface EventsByIdsResponse {
    events: Event[];
}