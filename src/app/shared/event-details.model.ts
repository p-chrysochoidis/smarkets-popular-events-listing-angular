import { Event } from "src/app/shared/event.model";

export class EventDetails {
    id: string;
    name: string;
    state: string;
    type: string;

    constructor(event: Event) {
        this.id = event.id;
        this.name = this.getEventName(event);
        this.state = event.state;
        this.type = event.type;
    }

    private getEventName(event: Event): string {
        return event.type === 'horse_racing_race' && event.short_name ? event.short_name : event.name;
    }
}