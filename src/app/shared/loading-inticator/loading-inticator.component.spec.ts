import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingInticatorComponent } from './loading-inticator.component';

describe('LoadingInticatorComponent', () => {
  let component: LoadingInticatorComponent;
  let fixture: ComponentFixture<LoadingInticatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingInticatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingInticatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
