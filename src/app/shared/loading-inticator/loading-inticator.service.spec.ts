import { TestBed } from '@angular/core/testing';

import { LoadingInticatorService } from './loading-inticator.service';

describe('LoadingInticatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoadingInticatorService = TestBed.get(LoadingInticatorService);
    expect(service).toBeTruthy();
  });
});
