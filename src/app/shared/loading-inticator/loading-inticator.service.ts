import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoadingInticatorService {
  private isLoaderVisible: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor() { }

  public get isLoading(): Observable<boolean> {
    return this.isLoaderVisible.pipe(delay(0));
  }

  public setLoading(isLoading: boolean) {
    this.isLoaderVisible.next(isLoading);
  }
}
