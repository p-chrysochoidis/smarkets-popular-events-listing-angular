export interface Market {
    bet_delay: number;
    category: string;
    complete: boolean;
    display_order: number;
    event_id: string;
    id: string;
    inplay_enabled: boolean;
    name: string;
    slug: string;
    state: string;
    volume: number;
    winner_count: number;
}