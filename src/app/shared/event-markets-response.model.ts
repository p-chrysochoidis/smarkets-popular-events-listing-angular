import { Market } from "./market.model";

export interface EventMarketsResponse {
    markets: Market[];
}