import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, retry } from 'rxjs/operators';
import { PopularEventIdsResponse } from './popular-event-ids-response.model';
import { EventsByIdsResponse } from './events-by-ids-response.model';
import { Event } from './event.model';
import { Market } from './market.model';
import { EventMarketsResponse } from './event-markets-response.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly RETRY_TIMES = 3;

  constructor(private http: HttpClient) { }

  public getPopularEventIds(): Observable<string[]> {
    return this.http.get<PopularEventIdsResponse>('/api/v3/popular/event_ids/', { params: {limit: '50'} })
      .pipe(
        map((response) => response.popular_event_ids || []),
        retry(this.RETRY_TIMES)
      );
  }

  public getEventsById(eventIds: string[]): Observable<Event[]> {
    return this.http.get<EventsByIdsResponse>(`/api/v3/events/${eventIds.join(',')}/`)
    .pipe(
      map((response) => response.events || []),
      retry(this.RETRY_TIMES)
    );
  }

  public getEventsMarkets(eventIds: string[]): Observable<Market[]> {
    return this.http.get<EventMarketsResponse>(`/api/v3/events/${eventIds.join(',')}/markets/`, { params: { limit: '50' } })
      .pipe(
        map((response) => response.markets || []),
        retry(this.RETRY_TIMES)
      )
  }
}
