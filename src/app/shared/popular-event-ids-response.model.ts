export interface PopularEventIdsResponse {
    popular_event_ids: string[];
}