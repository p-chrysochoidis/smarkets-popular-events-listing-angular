import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ApiService } from './api.service';

describe('ApiService', () => {
  let httpTestingController: HttpTestingController;
  let service: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(ApiService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getPopularEventIds', () => {
    it('should hit the correct endpoint', () => {
      service.getPopularEventIds().subscribe(() => {});
      httpTestingController.expectOne(popularEventsMatcher());
    });

    it('should default to an empty array', () => {
      let eventIds = null;
      service.getPopularEventIds().subscribe((eventIdsRes) => {
        eventIds = eventIdsRes;
      });
      let req = httpTestingController.expectOne(popularEventsMatcher());
      req.flush({});
      expect(eventIds).toEqual([]);
    });

    it('should return an array with ids', () => {
      let popular_event_ids = ['e1', 'e2', 'e3'];
      let eventIds = null;
      service.getPopularEventIds().subscribe((eventIdsRes) => {
        eventIds = eventIdsRes;
      });
      let req = httpTestingController.expectOne(popularEventsMatcher());
      req.flush({popular_event_ids});
      expect(eventIds).toEqual(popular_event_ids);
    });

    it('should retry 3 times before erroring', () => {
      let error = null;
      service.getPopularEventIds().subscribe(
        () => fail('should have failed'),
        (theError) => error = theError
      );
      let i = 4;
      while(i-- > 0) {
        let req = httpTestingController.expectOne(popularEventsMatcher());
        req.error(new ErrorEvent('An error', {message: 'Error Message'}));
      }
      expect(error.error.message).toEqual('Error Message');
    });
  });

  describe('getEventsById', () => {
    it('should hit the correct endpoint', () => {
      service.getEventsById(['e1']).subscribe(() => {});
      httpTestingController.expectOne(getEventsByIdMatcher(['e1']));
    });

    it('should default to an empty array', () => {
      let eventIds = ['e1', 'e2'];
      let events = null;
      service.getEventsById(eventIds).subscribe((eventsRes) => {
        events = eventsRes;
      });
      let req = httpTestingController.expectOne(getEventsByIdMatcher(eventIds));
      req.flush({});
      expect(events).toEqual([]);
    });

    it('should return an array with events', () => {
      let events = [
        makDummyEvent('e1'),
        makDummyEvent('e2'),
        makDummyEvent('e3')
      ];
      let eventIds = ['e1', 'e2', 'e3'];
      let eventsResult = null;
      service.getEventsById(eventIds).subscribe((eventsRes) => {
        eventsResult = eventsRes;
      });
      let req = httpTestingController.expectOne(getEventsByIdMatcher(eventIds));
      req.flush({events});
      expect(eventsResult).toEqual(events);
    });

    it('should retry 3 times before erroring', () => {
      let error = null;
      service.getEventsById([]).subscribe(
        () => fail('should have failed'),
        (theError) => error = theError
      );
      let i = 4;
      while(i-- > 0) {
        let req = httpTestingController.expectOne(getEventsByIdMatcher([]));
        req.error(new ErrorEvent('An error', {message: 'Error Message'}));
      }
      expect(error.error.message).toEqual('Error Message');
    });
  });

  function popularEventsMatcher() {
    return requestMatcher('GET', '/api/v3/popular/event_ids/');
  }

  function getEventsByIdMatcher(eventIds: string[]) {
    return requestMatcher('GET', `/api/v3/events/${eventIds.join(',')}/`);
  }

  function makDummyEvent(id: string) {
    return {id};
  }

  function requestMatcher(method: string, url: string) {
    return (req) => req.method === method && req.url === url;
  }
});
