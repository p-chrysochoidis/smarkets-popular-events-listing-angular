import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LoadingInticatorService } from './shared/loading-inticator/loading-inticator.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isLoading: Observable<boolean>;

  constructor(private loadingInticatorService: LoadingInticatorService) { }

  ngOnInit() {
    this.isLoading = this.loadingInticatorService.isLoading;
  }
}
