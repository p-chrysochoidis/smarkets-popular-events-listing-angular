import { Observable, Subject } from "rxjs";
import { Event } from "src/app/shared/event.model";

export class ApiServiceMock {
    private popularEventIdsStream: Subject<string[]> = new Subject();
    private eventsByIdStream: Subject<Event[]> = new Subject();

    public emmitEventsByIds(events: Event[]): void {
        this.eventsByIdStream.next(events);
    }

    public emmitPopularEventIds(eventIds: string[]): void {
        this.popularEventIdsStream.next(eventIds);
    }

    public getEventsById(): Observable<Event[]> {
        return this.eventsByIdStream;
    }

    public getPopularEventIds(): Observable<string[]> {
        return this.popularEventIdsStream;
    }
}