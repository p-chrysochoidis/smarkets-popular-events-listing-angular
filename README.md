# SmarketsPopularEventsListingAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.

## API info

The app expects the api to be at `domain/api/...`. So in order to develop/run it easily use docker/docker-compose as described below.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## ~~Running end-to-end tests~~

~~Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).~~

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Run using docker & docker-compose.

Requires docker and docker-compose to be installed.

## Run for development

From the top level directory run:
```
docker-compose -f ./docker/docker-compose.dev.yml up
```

## Run unit-tests

From the top level directory run:
```
docker-compose -f ./docker/docker-compose.unit-tests.yml up
```